package com.paywizard.agile.authorisation;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.paywizard.agile.authorisation.exception.UnauthorizedException;
import com.paywizard.agile.authorisation.togglz.FeaturesService;
import com.paywizard.agile.authorisation.togglz.util.FeaturesUtil;

@RestController
@SpringBootApplication
@RequestMapping(path = "authorisation/v1")
public class AuthorisationController
{
    FeaturesService mService = new FeaturesService();

    /**
     * Main entry point for the access token service.
     * 
     * @param args Any command line arguments passed in
     */
    public static void main(String[] args) 
    {
        SpringApplication.run(AuthorisationController.class, args);
    }

    // authorise?url=http://localhost:8080/cases/v1/create
    @GetMapping("/authorise")
    @ResponseStatus(HttpStatus.OK)
    public boolean authorize(@RequestParam(value="url") String aUrl, HttpServletResponse aResponse) throws IOException, UnauthorizedException
    {
        boolean lResult = false;

        String lService = FeaturesUtil.getServiceFromUrl(aUrl);

        System.out.println("CONTROLLER" + mService.getMatchingFeatures(lService, aUrl));

        lResult = mService.evaluate(lService, aUrl);

//        if (!lResult)
//        {
//            throw new UnauthorizedException("Unauthorized.");
//        }

        return lResult;
    }

    // authoriseFeature?feature=ACCESS_FEATURE
    @GetMapping("/authoriseFeature")
    @ResponseStatus(HttpStatus.OK)
    public boolean authorizeFeature(@RequestParam(value="feature") String aFeature, HttpServletResponse aResponse) throws IOException, UnauthorizedException
    {
        boolean lResult = false;
        System.out.println("Feature: " + aFeature);

        lResult = mService.evaluate(aFeature);
        if (!lResult)
        {
            throw new UnauthorizedException("Unauthorized.");
        }

        return lResult;
    }
}
