package com.paywizard.agile.authorisation.togglz.strategy;

import org.springframework.stereotype.Component;
import org.togglz.core.activation.Parameter;
import org.togglz.core.activation.ParameterBuilder;
import org.togglz.core.repository.FeatureState;
import org.togglz.core.user.FeatureUser;

@Component
public class ServiceActivationStrategy extends TenantActivationStrategy
{

	@Override
	public String getId()
	{
		return "service";
	}

	@Override
	public String getName()
	{
		return "Service";
	}

	@Override
	public Parameter[] getParameters() {
        return new Parameter[] {
                ParameterBuilder.create("tenantId").label("Tenant ID"),
        };
	}

	@Override
	public boolean isActive(FeatureState aFeatureState, FeatureUser aUser)
	{
        boolean lResult = super.isActive(aFeatureState, aUser);
        return lResult;
	}

}
