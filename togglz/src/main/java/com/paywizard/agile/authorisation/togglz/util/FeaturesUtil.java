package com.paywizard.agile.authorisation.togglz.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;

import org.togglz.core.Feature;
import org.togglz.core.context.FeatureContext;
import org.togglz.core.repository.FeatureState;

import com.paywizard.agile.authorisation.togglz.feature.MyFeatures;

public class FeaturesUtil
{
    private static final String SERVICE = "service";
    private static final String OPERATION = "operation";
	private static final String PATH = "path";

	/**
	 * 
	 * @param aUrl  http://localhost:8080/cases/v1/create
	 * 				http://localhost:8080/cases/v1/create/otherUrl
	 *              /cases/v1/create
	 *              /cases/v1/create/otherUrl
	 * @return
	 * @throws MalformedURLException 
	 */
	public static String getPathFromUrl(String aUrl) throws MalformedURLException
	{
		String lPath = "";
		if (aUrl.startsWith("http"))
		{
		      URL lUrl = new URL(aUrl);
		      lPath = lUrl.getPath();
		}
		else
		{
		    lPath = aUrl;
		}

        System.out.println("Path: " + lPath);
		return lPath;
	}

	public static String getServiceFromUrl(String aUrl) throws MalformedURLException
	{
		String lPath = getPathFromUrl(aUrl);
		String[] lPaths = lPath.split("/");
		return lPaths[1].toUpperCase();
	}

	/**
	 * 
	 * @param aService
	 * @param aUrl
	 * @return
	 * @throws MalformedURLException 
	 */
    public static Set<MyFeatures> getMatchingFeatures(Set<MyFeatures> lMyFeatures, String aPath) throws MalformedURLException
    {
    	Set<MyFeatures> lFeatures = new HashSet<MyFeatures>();
    	for (MyFeatures lFeature : lMyFeatures)
    	{

    		FeatureState lFeatureState = lFeature.getFeatureState();
            if (SERVICE.equals(lFeatureState.getStrategyId()))
            {
                lFeatures.add((MyFeatures) lFeature);
            }
            else if (OPERATION.equals(lFeatureState.getStrategyId()))
            {
                String lFeaturePath = lFeatureState.getParameter(PATH);
                lFeaturePath = lFeaturePath.replaceAll("/", "\\\\/");

                if (aPath.matches(lFeaturePath))
                {
                    System.out.println("Match aPath = " + aPath + " aFeaturePath = " + lFeaturePath);
                }
                else
                {
                    System.out.println("Not match aPath = " + aPath + " aFeaturePath = " + lFeaturePath);
                }
                
                String[] lFeaturePaths = lFeaturePath.split(",");
                for (String lFeaturePathItem : lFeaturePaths)
                {
                    if (aPath.matches(lFeaturePathItem))
                    {
                        System.out.println("Match aPath = " + aPath + " aFeaturePath = " + lFeaturePathItem);
                    }
                    else
                    {
                        System.out.println("Not match aPath = " + aPath + " aFeaturePath = " + lFeaturePathItem);
                    }

                    if (aPath.matches(lFeaturePathItem))
                    {
                        lFeatures.add((MyFeatures) lFeature);
                    }
                }
            }
    	}
    	return lFeatures;
    }
}
