package com.paywizard.agile.authorisation.togglz.feature.provider;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.togglz.core.Feature;
import org.togglz.core.metadata.FeatureGroup;
import org.togglz.core.metadata.FeatureMetaData;
import org.togglz.core.metadata.SimpleFeatureGroup;
import org.togglz.core.repository.FeatureState;
import org.togglz.core.util.Strings;

public class JDBCFeatureMetaData implements FeatureMetaData
{
    private String label;
    private final FeatureState defaultFeatureState;
    private final Set<FeatureGroup> groups = new HashSet<FeatureGroup>();
    boolean enabledByDefault = false;

    public JDBCFeatureMetaData(Feature feature, String specification)
    {
        if (Strings.isNotBlank(specification)) {
            String[] parts = specification.split(";");

            if (parts.length >= 1) {
                label = parts[0];
            }

            if (parts.length >= 2) {
                enabledByDefault = Boolean.parseBoolean(parts[1]);
            }

            if (parts.length >= 3) {
                groups.addAll(parseFeatureGroups(parts[2]));
            }

        }

        if (Strings.isBlank(label)) {
            label = feature.name();
        }

        this.defaultFeatureState = new FeatureState(feature, enabledByDefault);
    }

    private Set<FeatureGroup> parseFeatureGroups(String value) {
        Set<FeatureGroup> groups = new HashSet<FeatureGroup>();
        for (String label : value.split(",")) {
            if (Strings.isNotBlank(label)) {
                groups.add(new SimpleFeatureGroup(label.trim()));
            }
        }
        return groups;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public FeatureState getDefaultFeatureState() {
        return defaultFeatureState.copy();
    }

    @Override
    public Set<FeatureGroup> getGroups() {
        return groups;
    }

    @Override
    public Map<String, String> getAttributes() {
        // currently not supported by this implementation
        return Collections.emptyMap();
    }

}
