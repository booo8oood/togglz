package com.paywizard.agile.authorisation.togglz.feature.provider;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.togglz.core.Feature;
import org.togglz.core.metadata.FeatureMetaData;
import org.togglz.core.spi.FeatureProvider;
import org.togglz.core.util.DbUtils;
import org.togglz.core.util.NamedFeature;

import com.paywizard.agile.authorisation.togglz.feature.MyFeatures;

public class JDBCFeatureProvider implements FeatureProvider
{
    private DataSource dataSource;
    protected final String tableName;
    private final Set<Feature> features = new LinkedHashSet<Feature>();
    private final Map<String, FeatureMetaData> metadata = new HashMap<String, FeatureMetaData>();

    public JDBCFeatureProvider(DataSource dataSource, String tableName)
    {
        this.dataSource = dataSource;
        this.tableName = tableName;

        try
        {
        Connection connection = dataSource.getConnection();

        try {

            String sql = "SELECT FEATURE_NAME, FEATURE_ENABLED, STRATEGY_ID, STRATEGY_PARAMS FROM %TABLE%";
            PreparedStatement statement = connection.prepareStatement(insertTableName(sql));
            try {
                ResultSet resultSet = statement.executeQuery();
                try {
                    while (resultSet.next())
                    {

                        MyFeatures feature = new MyFeatures(resultSet.getString(1));
                        features.add(feature);
                        metadata.put(feature.name(), new JDBCFeatureMetaData(feature, resultSet.getString(1) + ";" +resultSet.getString(2) + ";" + resultSet.getString(3)));
                    }
   
                    System.out.println("FEATURES = " + features.toString());
                    System.out.println("METADATA = " + metadata.toString());
                } finally {
                    DbUtils.closeQuietly(resultSet);
                }

            } finally {
                DbUtils.closeQuietly(statement);
            }

        } finally {
            DbUtils.closeQuietly(connection);
        }

    } catch (SQLException e) {
        throw new IllegalStateException("Failed to fetch the feature's state from the database", e);
    }
    }


    private String insertTableName(String s)
    {
        return s.replace("%TABLE%", tableName);
    }


    @Override
    public Set<Feature> getFeatures() {
        return Collections.unmodifiableSet(features);
    }

    @Override
    public FeatureMetaData getMetaData(Feature feature) {
        return metadata.get(feature.name());
    }
}