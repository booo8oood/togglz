package com.paywizard.agile.authorisation.togglz;

import java.net.MalformedURLException;
import java.util.HashSet;
import java.util.Set;

import org.togglz.core.Feature;
import org.togglz.core.context.FeatureContext;
import org.togglz.core.manager.FeatureManager;
import org.togglz.core.repository.FeatureState;

import com.paywizard.agile.authorisation.togglz.feature.MyFeatures;
import com.paywizard.agile.authorisation.togglz.util.FeaturesUtil;

public class FeaturesService
{
    protected static final String SERVICE = "service";
    protected static final String OPERATION = "operation";
    protected static final String PARENT_SERVICE = "parentService";

    FeatureManager mFeatureManager;

    protected FeatureManager getFeatureManager()
    {
    	if (mFeatureManager == null)
    	{
    		mFeatureManager = FeatureContext.getFeatureManager();
    	}
		return mFeatureManager;
	}

    protected void setFeatureManager(FeatureManager aFeatureManager)
	{
		this.mFeatureManager = aFeatureManager;
	}

	protected Set<MyFeatures> getFeatures(String aService)
    {
    	Set<MyFeatures> lFeatures = new HashSet<MyFeatures>();
    	for (Feature lFeature : getFeatureManager().getFeatures())
    	{
    		System.out.println(lFeature);
    		FeatureState lFeatureState = ((MyFeatures) lFeature).getFeatureState();

    		if (lFeatureState == null)
    		{
    			lFeatureState =  getFeatureManager().getFeatureState(lFeature);
    		}

    		if (OPERATION.equals(lFeatureState.getStrategyId()))
    		{
        		String lParentService = lFeatureState.getParameter(PARENT_SERVICE);
        		if (aService.equals(lParentService))
        		{
        			MyFeatures lFeatureItem = (MyFeatures) lFeature;
        			lFeatureItem.setFeatureState(lFeatureState);
            		lFeatures.add(lFeatureItem);
        		}
    		}
    		if (SERVICE.equals(lFeatureState.getStrategyId()) && aService.equals(lFeature.name()))
    		{
                MyFeatures lFeatureItem = (MyFeatures) lFeature;
                lFeatureItem.setFeatureState(lFeatureState);
                lFeatures.add(lFeatureItem);
    		}
    	}
    	return lFeatures;
    }

    protected FeatureState getFeatureState(MyFeatures aFeature)
    {
    	FeatureState lFeatureState = getFeatureManager().getFeatureState(aFeature);
    	return lFeatureState;
    }

    public Set<MyFeatures> getMatchingFeatures(String aService, String aUrl) throws MalformedURLException
    {
    	Set<MyFeatures> lFeatures = getFeatures(aService);
    	return FeaturesUtil.getMatchingFeatures(lFeatures, FeaturesUtil.getPathFromUrl(aUrl));
    }

    public boolean evaluate(String aService, String aUrl) throws MalformedURLException
    {
    	boolean lResult = true;
    	Set<MyFeatures> lMyFeatures = getMatchingFeatures(aService, aUrl);
    	for (MyFeatures lFeature : lMyFeatures)
    	{
    		if (!lFeature.isActive(lFeature))
    		{
    			lResult = false;
    		}
    	}
    	return lResult;
    }

    public boolean evaluate(String aFeature) throws MalformedURLException
    {
        boolean lResult = true;
        MyFeatures lFeature = new MyFeatures(aFeature);
        if (!lFeature.isActive(lFeature))
        {
            lResult = false;
        }
        return lResult;
    }
}
