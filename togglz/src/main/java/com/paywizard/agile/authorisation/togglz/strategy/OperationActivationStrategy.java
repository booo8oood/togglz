package com.paywizard.agile.authorisation.togglz.strategy;

import org.springframework.stereotype.Component;
import org.togglz.core.activation.Parameter;
import org.togglz.core.activation.ParameterBuilder;
import org.togglz.core.repository.FeatureState;
import org.togglz.core.user.FeatureUser;

@Component
public class OperationActivationStrategy extends TenantActivationStrategy
{

	@Override
	public String getId()
	{
		return "operation";
	}

	@Override
	public String getName()
	{
		return "Operation";
	}

	@Override
	public Parameter[] getParameters()
	{
        return new Parameter[] {
                ParameterBuilder.create("tenantId").label("Tenant ID"),
                ParameterBuilder.create("parentService").label("Parent Service"),
                ParameterBuilder.create("path").label("Path")
        };
	}

	@Override
	public boolean isActive(FeatureState aFeatureState, FeatureUser aUser)
	{
	    boolean lResult = super.isActive(aFeatureState, aUser);
		return lResult;
	}

}
