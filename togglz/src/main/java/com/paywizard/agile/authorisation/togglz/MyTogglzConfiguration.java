package com.paywizard.agile.authorisation.togglz;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.togglz.core.repository.StateRepository;
import org.togglz.core.repository.jdbc.JDBCStateRepository;
import org.togglz.core.repository.util.DefaultMapSerializer;
import org.togglz.core.spi.FeatureProvider;
import org.togglz.core.user.FeatureUser;
import org.togglz.core.user.SimpleFeatureUser;
import org.togglz.core.user.UserProvider;

import com.paywizard.agile.authorisation.togglz.feature.provider.JDBCFeatureProvider;

@Component
public class MyTogglzConfiguration
{
	@Autowired
	private DataSource dataSource;

	@Autowired
	private HttpServletRequest request;

    @Bean
    public FeatureProvider featureProvider()
    {
//        return new EnumBasedFeatureProvider(MyFeatures.class);
        return new JDBCFeatureProvider(dataSource, "features");
    }

	@Bean
	public UserProvider userProvider()
	{
		return new UserProvider()
		{
			@Override
			public FeatureUser getCurrentUser()
			{
				System.out.println("UserProvider is invoked.");
				// Get the information from the User Context
				String lUsername = "test";
				boolean lIsAdmin = true;

			    Enumeration<String> headers = request.getHeaderNames();
			    while (headers.hasMoreElements())
			    {
			    	String header = headers.nextElement();
				    System.out.println("Header:"+header+"\tValue:"+request.getHeader(header));
			    }

				String lTenantId = request.getHeader("TENANT_ID");
				SimpleFeatureUser lUser = new SimpleFeatureUser(lUsername, lIsAdmin);
				lUser.setAttribute("tenantId", lTenantId);
				return lUser;
			}
		};
	}

	@Bean
	public StateRepository stateRepository()
	{
		 StateRepository repository = JDBCStateRepository.newBuilder(dataSource)
			     .tableName("features")
			     .createTable(false)
			     .serializer(DefaultMapSerializer.singleline())
			     .noCommit(true)
			     .build();
		return repository;
	}
}
