package com.paywizard.agile.authorisation.togglz.strategy;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.togglz.core.activation.Parameter;
import org.togglz.core.activation.ParameterBuilder;
import org.togglz.core.repository.FeatureState;
import org.togglz.core.spi.ActivationStrategy;
import org.togglz.core.user.FeatureUser;

@Component
public class TenantActivationStrategy implements ActivationStrategy
{

    @Override
    public String getId()
    {
        return "tenant";
    }

    @Override
    public String getName()
    {
        return "Tenant";
    }

    @Override
    public Parameter[] getParameters()
    {
        return new Parameter[] {
                ParameterBuilder.create("tenantId").label("Tenant ID"),
        };
    }

    @Override
    public boolean isActive(FeatureState aFeatureState, FeatureUser aUser)
    {
        String lTenantId = (String) aUser.getAttribute("tenantId");
        String lTenant = aFeatureState.getParameter("tenantId");
        boolean lResult = false;

        if (!StringUtils.isEmpty(lTenant))
        {
            String[] lTenantArray = lTenant.split(",");
            System.out.println(lTenantArray);
            List<String>  lTenantList = (List<String>) Arrays.asList(lTenantArray);
            System.out.println(lTenantList.toString());

            System.out.println(aFeatureState.getFeature().name());
            if (lTenantList.contains(lTenantId))
            {
                System.out.println("contains");
                lResult = true;
            }
        }
        return lResult;
    }

}
