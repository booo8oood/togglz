package com.paywizard.agile.authorisation.togglz.feature;

import org.togglz.core.context.FeatureContext;
import org.togglz.core.repository.FeatureState;
import org.togglz.core.util.NamedFeature;

public class MyFeatures extends NamedFeature
{
    private static final long serialVersionUID = 891792392952423044L;
	private FeatureState mFeatureState;

	public MyFeatures(String aName)
	{
	    super(aName);
	}
	
	public MyFeatures(String aName, FeatureState aFeatureState)
	{
	    super(aName);
	    this.mFeatureState = aFeatureState;
	}

	public void setFeatureState(FeatureState aFeatureState)
	{
		mFeatureState = aFeatureState;
	}

	public FeatureState getFeatureState()
	{
		return mFeatureState;
	}

	public boolean isActive(MyFeatures aFeature)
	{
		return FeatureContext.getFeatureManager().isActive(aFeature);
	}
}
