package com.paywizard.agile.authorisation.togglz.util;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.togglz.core.repository.FeatureState;

import com.paywizard.agile.authorisation.togglz.feature.MyFeatures;
import com.paywizard.agile.authorisation.togglz.util.FeaturesUtil;

@RunWith(MockitoJUnitRunner.class)
public class FeaturesUtilTest
{
	
	@Test
	public void testGetPathFromUrl() throws MalformedURLException
	{
		String lUrl = "http://localhost:8080/cases/v1/create";
		String lPath = FeaturesUtil.getPathFromUrl(lUrl);
		assertEquals("/cases/v1/create", lPath);
	}

	@Test
	public void testGetServiceFromUrl() throws MalformedURLException
	{
		String lUrl = "http://localhost:8080/cases/v1/create";
		String lService = FeaturesUtil.getServiceFromUrl(lUrl);
		assertEquals("CASES", lService);
	}

	@Test
	public void testGetMatchingFeatures() throws MalformedURLException
	{
		String lServiceName = "CASES";
		String lUrl = "http://localhost:8080/cases/v1/create";

		MyFeatures lMyFeatures0 = new MyFeatures("CASES_ALL");
	    MyFeatures lMyFeatures1 = new MyFeatures("CASES_CREATE");
	    MyFeatures lMyFeatures2 = new MyFeatures("CASES_OPERATION");
	    MyFeatures[] lMyFeatures = {lMyFeatures0, lMyFeatures1, lMyFeatures2};

		FeatureState lCasesAllFeatureState = new FeatureState(lMyFeatures[0]);
		lCasesAllFeatureState.setStrategyId("operation");
		lCasesAllFeatureState.setParameter("parentService", lServiceName);
		lCasesAllFeatureState.setParameter("path", "/cases/v1/.*");
		lMyFeatures[0].setFeatureState(lCasesAllFeatureState);

		FeatureState lCasesCreateFeatureState = new FeatureState(lMyFeatures[1]);
		lCasesCreateFeatureState.setStrategyId("operation");
		lCasesCreateFeatureState.setParameter("parentService", lServiceName);
		lCasesCreateFeatureState.setParameter("path", "/cases/v1/create.*");
		lMyFeatures[1].setFeatureState(lCasesCreateFeatureState);

		FeatureState lCasesOperationFeatureState = new FeatureState(lMyFeatures[2]);
		lCasesOperationFeatureState.setStrategyId("operation");
		lCasesOperationFeatureState.setParameter("parentService", lServiceName);
		lCasesOperationFeatureState.setParameter("path", "/cases/v1/create.*,/cases/v1/delete.*");
		lMyFeatures[2].setFeatureState(lCasesOperationFeatureState);

		Set<MyFeatures> lMyFeaturesSet = new HashSet<MyFeatures>(Arrays.asList(lMyFeatures));
		Set<MyFeatures> lResultSet = FeaturesUtil.getMatchingFeatures(lMyFeaturesSet, "/cases/v1/create");
		assertEquals(3, lResultSet.size());

		lResultSet = FeaturesUtil.getMatchingFeatures(lMyFeaturesSet, "/cases/v1/create/test");
		assertEquals(3, lResultSet.size());

		lResultSet = FeaturesUtil.getMatchingFeatures(lMyFeaturesSet, "/cases/v1/delete");
		assertEquals(2, lResultSet.size());
	}
}
