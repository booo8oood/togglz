package com.paywizard.agile.authorisation.togglz;

import static org.mockito.Mockito.*;

import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.togglz.core.Feature;
import org.togglz.core.manager.FeatureManager;
import org.togglz.core.repository.FeatureState;

import com.paywizard.agile.authorisation.togglz.FeaturesService;
import com.paywizard.agile.authorisation.togglz.feature.MyFeatures;

@RunWith(MockitoJUnitRunner.class)
public class FeaturesServiceTest
{
	FeaturesService lService = new FeaturesService();

	@Mock
	FeatureManager mFeatureManager;

	@Before
	public void before()
	{
		lService.setFeatureManager(mFeatureManager);
	}

	@Test
	public void testGetFeatures()
	{
		String lServiceName = "CASES";
	    MyFeatures lMyFeatures0 = new MyFeatures("CASES");
	    MyFeatures lMyFeatures1 = new MyFeatures("CASES_ALL");
	    MyFeatures lMyFeatures2 = new MyFeatures("CASES_CREATE");
	    MyFeatures lMyFeatures3 = new MyFeatures("CASES_OPERATION");
	    MyFeatures lMyFeatures4 = new MyFeatures("DOCUMENTS");
	    MyFeatures lMyFeatures5 = new MyFeatures("DOCUMENTS_ALL");
	    
		MyFeatures[] lMyFeatures = {lMyFeatures0, lMyFeatures1, lMyFeatures2, lMyFeatures3, lMyFeatures4, lMyFeatures5};

		FeatureState lCasesFeatureState = new FeatureState(lMyFeatures[0]);
		lCasesFeatureState.setStrategyId(FeaturesService.SERVICE);
		lMyFeatures[0].setFeatureState(lCasesFeatureState);

		FeatureState lCasesAllFeatureState = new FeatureState(lMyFeatures[1]);
		lCasesAllFeatureState.setParameter("parentService", "CASES");
		lCasesAllFeatureState.setParameter("path", "/cases/v1/.*");
		lCasesAllFeatureState.setStrategyId(FeaturesService.OPERATION);
		lMyFeatures[1].setFeatureState(lCasesAllFeatureState);

		FeatureState lCasesCreateFeatureState = new FeatureState(lMyFeatures[2]);
		lCasesCreateFeatureState.setParameter("parentService", "CASES");
		lCasesCreateFeatureState.setParameter("path", "/cases/v1/create.*");
		lCasesCreateFeatureState.setStrategyId(FeaturesService.OPERATION);
		lMyFeatures[2].setFeatureState(lCasesCreateFeatureState);

		FeatureState lCasesOperationFeatureState = new FeatureState(lMyFeatures[3]);
		lCasesOperationFeatureState.setParameter("parentService", "CASES");
		lCasesOperationFeatureState.setParameter("path", "/cases/v1/create.*,/cases/v1/delete.*");
		lCasesOperationFeatureState.setStrategyId(FeaturesService.OPERATION);
		lMyFeatures[3].setFeatureState(lCasesOperationFeatureState);

		FeatureState lDocumentsFeatureState = new FeatureState(lMyFeatures[4]);
		lDocumentsFeatureState.setStrategyId(FeaturesService.SERVICE);
		lMyFeatures[4].setFeatureState(lDocumentsFeatureState);

		FeatureState lDocumentsAllFeatureState = new FeatureState(lMyFeatures[5]);
		lDocumentsAllFeatureState.setParameter("parentService", "DOCUMENTS");
		lDocumentsAllFeatureState.setParameter("path", "/documents/v1/.*");
		lDocumentsAllFeatureState.setStrategyId(FeaturesService.OPERATION);
		lMyFeatures[5].setFeatureState(lDocumentsAllFeatureState);

		Set<MyFeatures> lMyFeaturesSet = new HashSet<MyFeatures>(Arrays.asList(lMyFeatures));
		Set<Feature> lfeaturesSet = new HashSet<Feature>(Arrays.asList(lMyFeatures));

		when(mFeatureManager.getFeatures()).thenReturn(lfeaturesSet);

		System.out.println("SERVICE" + lServiceName + " " + lService.getFeatures(lServiceName));
	}

	@Test
	public void testGetMatchingFeatures() throws MalformedURLException
	{
		String lServiceName = "CASES";
		String lUrl = "http://localhost:8080/cases/v1/delete";
        MyFeatures lMyFeatures0 = new MyFeatures("CASES");
        MyFeatures lMyFeatures1 = new MyFeatures("CASES_ALL");
        MyFeatures lMyFeatures2 = new MyFeatures("CASES_CREATE");
        MyFeatures lMyFeatures3 = new MyFeatures("CASES_OPERATION");
        MyFeatures lMyFeatures4 = new MyFeatures("DOCUMENTS");
        MyFeatures lMyFeatures5 = new MyFeatures("DOCUMENTS_ALL");
        
        MyFeatures[] lMyFeatures = {lMyFeatures0, lMyFeatures1, lMyFeatures2, lMyFeatures3, lMyFeatures4, lMyFeatures5};

		FeatureState lCasesFeatureState = new FeatureState(lMyFeatures[0]);
		lCasesFeatureState.setStrategyId(FeaturesService.SERVICE);
		lMyFeatures[0].setFeatureState(lCasesFeatureState);

		FeatureState lCasesAllFeatureState = new FeatureState(lMyFeatures[1]);
		lCasesAllFeatureState.setParameter("parentService", "CASES");
		lCasesAllFeatureState.setParameter("path", "/cases/v1/.*");
		lCasesAllFeatureState.setStrategyId(FeaturesService.OPERATION);
		lMyFeatures[1].setFeatureState(lCasesAllFeatureState);

		FeatureState lCasesCreateFeatureState = new FeatureState(lMyFeatures[2]);
		lCasesCreateFeatureState.setParameter("parentService", "CASES");
		lCasesCreateFeatureState.setParameter("path", "/cases/v1/create.*");
		lCasesCreateFeatureState.setStrategyId(FeaturesService.OPERATION);
		lMyFeatures[2].setFeatureState(lCasesCreateFeatureState);

		FeatureState lCasesOperationFeatureState = new FeatureState(lMyFeatures[3]);
		lCasesOperationFeatureState.setParameter("parentService", "CASES");
		lCasesOperationFeatureState.setParameter("path", "/cases/v1/create.*,/cases/v1/delete.*");
		lCasesOperationFeatureState.setStrategyId(FeaturesService.OPERATION);
		lMyFeatures[3].setFeatureState(lCasesOperationFeatureState);

		FeatureState lDocumentsFeatureState = new FeatureState(lMyFeatures[4]);
		lDocumentsFeatureState.setStrategyId(FeaturesService.SERVICE);
		lMyFeatures[4].setFeatureState(lDocumentsFeatureState);

		FeatureState lDocumentsAllFeatureState = new FeatureState(lMyFeatures[5]);
		lDocumentsAllFeatureState.setParameter("parentService", "DOCUMENTS");
		lDocumentsAllFeatureState.setParameter("path", "/documents/v1/.*");
		lDocumentsAllFeatureState.setStrategyId(FeaturesService.OPERATION);
		lMyFeatures[5].setFeatureState(lDocumentsAllFeatureState);

		Set<MyFeatures> lMyFeaturesSet = new HashSet<MyFeatures>(Arrays.asList(lMyFeatures));
		Set<Feature> lfeaturesSet = new HashSet<Feature>(Arrays.asList(lMyFeatures));

		when(mFeatureManager.getFeatures()).thenReturn(lfeaturesSet);
		System.out.println("SERVICE" + lUrl + " " + lService.getMatchingFeatures(lServiceName, lUrl));
	}
}
