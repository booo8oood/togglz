Setup datasource:
CREATE TABLE <table> (
   FEATURE_NAME VARCHAR(100) PRIMARY KEY, 
   FEATURE_ENABLED INTEGER, 
   STRATEGY_ID VARCHAR(200), 
   STRATEGY_PARAMS VARCHAR(2000)
 )

Set the datasource in application.properties
e.g. Oracle datasource

#Basic Spring Boot Config for Oracle
spring.datasource.url=jdbc:oracle:thin:@//localhost:1521/xe
spring.datasource.username=enterprisepb
spring.datasource.password=enterprisepb
spring.datasource.driver-class-name=oracle.jdbc.OracleDriver

Compile: mvn -U package eclipse:clean eclipse:eclipse
Run: mvn spring-boot:run

To add a Feature:
Add feature in enum MyFeatures.java

To manage Features:
http://localhost:8080/togglz

Execution:
http://localhost:8080/api/greeting
* Add valid tenantId in request header