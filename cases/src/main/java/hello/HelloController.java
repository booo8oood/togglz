package hello;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.CaseDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(path = "cases/v1")
public class HelloController
{
    @Autowired
    private HttpServletRequest request;

    Set<CaseDTO> mCaseList = new HashSet<CaseDTO>();

    @Value("${authorisation-service.endpoint}")
    private String authorisationServiceEndpoint;

    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @GetMapping(value="/cases", produces="application/json")
    public Set<CaseDTO> retrieveAll(HttpServletRequest request, HttpServletResponse response)
    {
        Set<CaseDTO> lCaseResultList = null;
        if (mCaseList.size() == 0)
        {
            CaseDTO lCase1 = new CaseDTO();
            lCase1.setId(1);
            lCase1.setName("Case1");
            lCase1.setDescription("Case1Description");

            CaseDTO lCase2 = new CaseDTO();
            lCase2.setId(2);
            lCase2.setName("Case2");
            lCase2.setDescription("Case2Description");
            
            CaseDTO lCase3 = new CaseDTO();
            lCase3.setId(3);
            lCase3.setName("Case3");
            lCase3.setDescription("Case3Description");

            CaseDTO lCase4 = new CaseDTO();
            lCase4.setId(4);
            lCase4.setName("Case4");
            lCase4.setDescription("Case4Description");

            CaseDTO lCase5 = new CaseDTO();
            lCase5.setId(5);
            lCase5.setName("Case5");
            lCase5.setDescription("Case5Description");
            mCaseList.add(lCase1);
            mCaseList.add(lCase2);
            mCaseList.add(lCase3);
            mCaseList.add(lCase4);
            mCaseList.add(lCase5);
        }
        
        // Retrieve TENANT_ID from the header
        String lTenantId = request.getHeader("TENANT_ID");
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("TENANT_ID", lTenantId);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        String url = String.format(authorisationServiceEndpoint, "CASES_UPPERCASE");

        ResponseEntity<String> lAuthorisationResponse = restTemplate.exchange(url, HttpMethod.GET, entity, String.class, "");
        
        String lResult = lAuthorisationResponse.getBody();

        lCaseResultList = mCaseList;
        if (Boolean.TRUE.toString().equals(lResult))
        {
            Set<CaseDTO> lCaseList = new HashSet<CaseDTO>();
            for (CaseDTO lCase : mCaseList)
            {
                CaseDTO lNewCase = lCase.copy();
                lNewCase.setName(lCase.getName().toUpperCase());
                lNewCase.setDescription(lCase.getDescription().toUpperCase());
                lCaseList.add(lNewCase);
            }
            lCaseResultList = lCaseList;
        }

        System.out.println("RETRIEVE_ALL: ");
        return lCaseResultList;
    }
    
    @GetMapping(value="/{id}", produces="application/json")
    public CaseDTO retrieve(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") Integer id)
    {
        System.out.println("RETRIEVE: " + id);
        CaseDTO lResult = null;
        for (CaseDTO lCase : mCaseList)
        {
            if (lCase.getId() == id)
            {
                lResult = lCase;
            }
        }
        // Retrieve TENANT_ID from the header
        String lTenantId = "ITV";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("TENANT_ID", lTenantId);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        String url = String.format(authorisationServiceEndpoint, "CASES_UPPERCASE");

        ResponseEntity<String> lAuthorisationResponse = restTemplate.exchange(url, HttpMethod.GET, entity, String.class, "");
        
        String lAuthorisationResult = lAuthorisationResponse.getBody();
        if (Boolean.TRUE.toString().equals(lAuthorisationResult))
        {
            lResult.setName(lResult.getName().toUpperCase());
            lResult.setDescription(lResult.getDescription().toUpperCase());
        }
        return lResult;
    }
 
    @PostMapping(value="/create", consumes="application/json", produces="application/json")
    public void create(@RequestBody CaseDTO aCase)
    {
        int lastId = 0;
        for (CaseDTO lCase : mCaseList)
        {
            if (lCase.getId() > lastId)
            {
                lastId = lCase.getId();
            }
        }
        aCase.setId(++lastId);
        System.out.println("CREATE: " + aCase.getId() + " " + aCase.getName());
        mCaseList.add(aCase);
    }

    @DeleteMapping(value="/delete/{id}")
    public void delete(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") Integer id)
    {
        System.out.println("DELETE: " + id);
        CaseDTO lCase = retrieve(request, response, id);
        mCaseList.remove(lCase);
    }
}
