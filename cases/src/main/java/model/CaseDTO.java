package model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value = "case")
public class CaseDTO
{
    private int id;

    private String name;

    private String description;
 
    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @Override
    public int hashCode()
    {
        return this.id;
    }

    @Override
    public boolean equals(Object aCase)
    {
        return this.id == ((CaseDTO) aCase).getId();
    }

    public CaseDTO copy()
    {
        CaseDTO lCase = new CaseDTO();
        lCase.setId(this.id);
        lCase.setName(this.name);
        lCase.setDescription(this.description);
        return lCase;
    }
    
}
